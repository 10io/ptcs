# frozen_string_literal: true

module Ptcs
  module Coverage
    class Analyzer
      attr_reader :xml, :stats

      def initialize(file)
        @xml = Nokogiri.XML(file)
        @stats = Coverage::Stats.new
      end

      def execute!
        files = xml.css('tr.t-file')

        files.each do |entry|
          coverage_entry = coverage_stats_of(entry)
          if package_code?(coverage_entry[:path])
            @stats.append(**coverage_entry)
          else
            entry.remove
          end
        end
        @stats.finalize!
      end

      private

      def coverage_stats_of(entry)
        lines_count = entry.css('.cell--number')
        {
          path: entry.css('.t-file__name > a').text,
          coverage: Float(entry.css('.t-file__coverage').text.gsub(' %', '')),
          lines: Integer(lines_count[1].text),
          relevant_lines: Integer(lines_count[2].text),
          covered_lines: Integer(lines_count[3].text),
          missed_lines: Integer(lines_count[4].text)
        }
      end

      def package_code?(path)
        unless path.include?('package') || path.include?('container') || path.include?('dependency_prox') || path.include?('/go_')
          return false
        end

        if path.include?('/geo/') ||
            path.include?('db/') ||
            path.include?('/security/') ||
            path.include?('container_scanning') ||
            path.include?('base_container_service') ||
            path.include?('vulnerab') ||
            path.include?('background_migration') ||
            path.include?('dependency_link')
          return false
        end

        true
      end
    end
  end
end
