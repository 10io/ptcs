# frozen_string_literal: true

module Ptcs
  module Coverage
    class Stats
      attr_reader :entries

      DETAILS_TABLE_HEADERS = %w[file coverage lines relevant_lines covered_lines missed_lines].freeze
      SUMMARY_TABLE_HEADERS = %w[total_coverage total_lines total_relevant_lines total_covered_lines total_missed_lines]

      def initialize
        @entries = []
      end

      def append(path:, coverage:, lines:, relevant_lines:, covered_lines:, missed_lines:)
        @entries << Stat.new(path: path, coverage: coverage, lines: lines, relevant_lines: relevant_lines, covered_lines: covered_lines, missed_lines: missed_lines)
      end

      def finalize!
        @entries.sort_by!(&:coverage)
        @entries.uniq!(&:path)
      end

      def render_sorted
        render_entries(@entries)
      end

      def render_top_ten
        render_entries(@entries.first(10))
      end

      def render_summary
        total_coverage = (sum(:coverage) / @entries.size).round(2)
        table = TTY::Table.new(
          header: SUMMARY_TABLE_HEADERS,
          rows: [
            [
              "#{total_coverage} %",
              sum(:lines),
              sum(:relevant_lines),
              sum(:covered_lines),
              sum(:missed_lines)
            ]
          ]
        )
        table.render(:ascii)
      end

      private

      def sum(field)
        @entries.map(&field).reduce(:+)
      end

      def render_entries(entries)
        table = TTY::Table.new(header: DETAILS_TABLE_HEADERS, rows: entries.map(&:to_tty_table_entry))
        table.render(:ascii)
      end
    end
  end
end
