# frozen_string_literal: true

module Ptcs
  module Coverage
    class Stat
      attr_reader :path, :coverage, :lines, :relevant_lines, :covered_lines, :missed_lines

      def initialize(path:, coverage:, lines:, relevant_lines:, covered_lines:, missed_lines:)
        @path = path
        @coverage = coverage
        @lines = lines
        @relevant_lines = relevant_lines
        @covered_lines = covered_lines
        @missed_lines = missed_lines
      end

      def to_tty_table_entry
        [path, humanized_coverage, lines, relevant_lines, covered_lines, missed_lines]
      end

      private

      def humanized_coverage
        "#{coverage} %"
      end
    end
  end
end
