# frozen_string_literal: true

require 'forwardable'

module Ptcs
  class Command
    extend Forwardable

    def_delegators :command, :run

    # Execute this command
    #
    # @api public
    def execute(*)
      raise(
        NotImplementedError,
        "#{self.class}##{__method__} must be implemented"
      )
    end

    def logger
      @logger ||= ::TTY::Logger.new
    end
  end
end
