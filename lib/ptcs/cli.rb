# frozen_string_literal: true

require 'thor'
require 'tty-logger'
require 'tty-file'
require 'tty-table'
require 'nokogiri'
require_relative 'coverage/analyzer'
require_relative 'coverage/stats'
require_relative 'coverage/stat'

module Ptcs
  # Handle the application command line parsing
  # and the dispatch to various command objects
  #
  # @api public
  class CLI < Thor
    # Error raised by this runner
    Error = Class.new(StandardError)

    DEFAULT_URL = 'https://gitlab-org.gitlab.io/gitlab/coverage-ruby/index.html'

    desc 'version', 'ptcs version'
    def version
      require_relative 'version'
      puts "v#{Ptcs::VERSION}"
    end
    map %w(--version -v) => :version

    desc 'backend [URL]', "Retrieve the coverage report from [URL](defaults to #{DEFAULT_URL}) and update it to only include Package related code"
    method_option :help,
                  aliases: '-h',
                  type: :boolean,
                  desc: 'Display usage information'
    method_option :overview,
                  aliases: '-o',
                  type: :boolean,
                  desc: 'Outputs a coverage report overview in the console'
    method_option :details,
                  aliases: '-d',
                  type: :boolean,
                  desc: 'Outputs a coverage detailed report in the console'
    def backend(url = DEFAULT_URL)
      if options[:help]
        invoke :help, ['backend']
      else
        require_relative 'commands/backend'
        Ptcs::Commands::Backend.new(url, options).execute
      end
    end
  end
end
