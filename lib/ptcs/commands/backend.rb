# frozen_string_literal: true

require_relative '../command'
require 'open-uri'
require 'fileutils'

module Ptcs
  module Commands
    class Backend < Ptcs::Command
      attr_reader :url

      COVERAGE_FOLDER = 'coverage'
      COVERAGE_FILE = 'index.html'
      ASSETS_FOLDER_PATH = 'lib/ptcs/templates/backend/coverage/assets'
      ASSETS_FOLDER = 'assets'

      def initialize(url, options)
        @url = url
        @options = options
      end

      def execute(input: $stdin, output: $stdout)
        setup_coverage_folder
        fetch
        analyze

        case mode
        when :details
          details
          summary
        when :overview
          top_ten
          summary
        else
          generate
        end

        logger.success('All done!')
      end

      private

      def mode
        return :details if @options[:details]
        return :overview if @options[:overview]
        :generation
      end

      def setup_coverage_folder
        logger.success('Setup coverage folder')

        ::FileUtils.remove_entry(COVERAGE_FOLDER) if File.directory?(COVERAGE_FOLDER)
        ::FileUtils.mkdir(COVERAGE_FOLDER)
        TTY::File.copy_dir(ASSETS_FOLDER_PATH, File.join(COVERAGE_FOLDER, ASSETS_FOLDER)) if mode == :generation

        logger.success('Coverage folder setup done!')
      end

      def fetch
        logger.info("Getting coverage file from #{url}")

        filepath = File.join(COVERAGE_FOLDER, COVERAGE_FILE)
        TTY::File.download_file(url, filepath)

        logger.success('Coverage file downloaded!')
        @file = File.new(filepath)
      end

      def analyze
        logger.info('Analyzing coverage file')

        @analyzer = Coverage::Analyzer.new(@file).tap(&:execute!)

        logger.success('Coverage file analyzed!')
      end

      def details
        table = @analyzer.stats.render_sorted

        logger.success("Coverage Details")
        logger.success("\n#{table}")
      end

      def summary
        table = @analyzer.stats.render_summary

        logger.success("Coverage Summary")
        logger.success("\n#{table}")
      end

      def top_ten
        table = @analyzer.stats.render_top_ten

        logger.success("Top 10 Worst Coverage Summary")
        logger.success("\n#{table}")
      end

      def generate
        logger.info('Generating new coverage file')

        File.truncate(@file.path, 0)
        TTY::File.append_to_file(@file.path, @analyzer.xml.to_html)

        logger.success('Coverage file generated!')
        logger.info("Open #{@file.path} with your browser")
      end
    end
  end
end
