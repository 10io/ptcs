# Package Team Coverage Stats

Use this tool to extract coverage stats from the gitlab coverage report tailored for the [GitLab Package Team](https://about.gitlab.com/handbook/engineering/development/ops/package/).

# Installation

Requirement:
* Bundler

```shell
$ ./bin/setup
```

# Available coverages

## Backend

Coverage source will be the url passed as argument or https://gitlab-org.gitlab.io/gitlab/coverage-ruby/index.html by default.

```shell
$ ./bin/ptcs backend -h
Usage:
  ptcs backend [URL]

Options:
  -h, [--help], [--no-help]          # Display usage information
  -o, [--overview], [--no-overview]  # Outputs a coverage report overview in the console
  -d, [--details], [--no-details]    # Outputs a coverage detailed report in the console

Retrieve the coverage report from [URL](defaults to https://gitlab-org.gitlab.io/gitlab/coverage-ruby/index.html) and update it to only include Package related code
```

#### Clone the report

The original report is cloned and all the coverage stats relevant to the [GitLab Package Team](https://about.gitlab.com/handbook/engineering/development/ops/package/) are removed.
An updated report is then created and can be browsed.

:caution: Coverages on the tabs and stats around the title of each tab is from the original report. Only the file listing is updated. :caution:

```shell
./bin/ptcs backend
```

#### Overview (console only)

The original report is cloned and all the coverage stats relevant to the [GitLab Package Team](https://about.gitlab.com/handbook/engineering/development/ops/package/) are removed.
All entries are analyzed and an overview is printed on the console.

```shell
./bin/ptcs backend -o
```

#### Details (console only)

The original report is cloned and all the coverage stats relevant to the [GitLab Package Team](https://about.gitlab.com/handbook/engineering/development/ops/package/) are removed.
All entries are analyzed and an detailed report (eg. all files) is printed on the console.

```shell
./bin/ptcs backend -d
```

# TODOs

* Where are my tests?
* Fix the ascii array on CI jobs. Plz. k? thx.
* Fancy console outputs with [pastel](https://github.com/piotrmurach/pastel)
