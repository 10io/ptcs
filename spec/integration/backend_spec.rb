RSpec.describe "`ptcs backend` command", type: :cli do
  it "executes `ptcs help backend` command successfully" do
    output = `ptcs help backend`
    expected_output = <<-OUT
Usage:
  ptcs backend URL

Options:
  -h, [--help], [--no-help]  # Display usage information

Command description...
    OUT

    expect(output).to eq(expected_output)
  end
end
