require 'ptcs/commands/backend'

RSpec.describe Ptcs::Commands::Backend do
  it "executes `backend` command successfully" do
    output = StringIO.new
    url = nil
    options = {}
    command = Ptcs::Commands::Backend.new(url, options)

    command.execute(output: output)

    expect(output.string).to eq("OK\n")
  end
end
